from ...breakout.sprites.paddle import Paddle
from ...breakout.sprites.ball import Ball
from ...breakout.game import Game
from ...breakout.screen_manager import ScreenManager
from ..constants.game_constants import *
from ..constants.screen_constants import *

class CollisionDetection():
    def __init__(self):
        self.screenManager = ScreenManager()
        
    def check_wall_collision(self, ball, lives, paddle, game):
        #Check if the ball is bouncing against any of the 4 walls:
        if ball.rect.x >= (SCREEN_WIDTH - (BALL_WIDTH + 2)) and ball.velocity[0] > 0:
            ball.velocity[0] = -ball.velocity[0]
        if ball.rect.x <= 0 and ball.velocity[0] < 0:
            ball.velocity[0] = -ball.velocity[0]
        if ball.rect.y > (SCREEN_HEIGHT - (BALL_HEIGHT + 2)):
            ball.velocity[1] = -ball.velocity[1]
            lives -= 1
            self.__add_AI_punishment(ball, paddle, game)
        if ball.rect.y < (SCREEN_HEIGHT / 21.6) and ball.velocity[1] < 0:
            ball.velocity[1] = -ball.velocity[1]
        return lives

    def check_bricks_collision(self, brick_collision_list, ball, game: Game, all_bricks, pygame, screen):
        for brick in brick_collision_list:
            self.__check_brick_collision(ball, brick)
          
            game.score += brick.points
            brick.kill()
            game.reward += BRICK_COLLISION_REWARD
            game.time_since_last_brick_hit = 0
            if len(all_bricks) == 0:
                if game.level == LEVELS:
                    return True
                game.reward += LEVEL_COMPLETE_REWARD
                self.screenManager.level_complete(screen, pygame, game)
                return True
        return False

    def check_paddle_collision(self, pygame, ball: Ball, paddle: Paddle, game: Game):
        #Detect collisions between the ball and the paddles
        if pygame.sprite.collide_mask(ball, paddle):
            game.reward += PADDLE_COLLISION_REWARD
            pX = paddle.rect.x
            bX = ball.rect.x
            distance = bX - pX

            margin = PADDLE_WIDTH / 10 # was 200

            if (distance < (margin*2)):
                ball.bounce(-8 * BALL_SPEED)

            if (distance >= (margin*2) and distance < (margin*4)):
                ball.bounce(-4 * BALL_SPEED)

            if (distance >= (margin*4) and distance < (margin*6)):
                ball.bounce(1)

            if (distance >= (margin*6) and distance <= (margin*8)):
                ball.bounce(4 * BALL_SPEED)

            if (distance > (margin*8)):
                ball.bounce(8 * BALL_SPEED)

        if GIVE_REWARD_BASED_ON_DISTANCE and (ball.rect.top < paddle.rect.bottom):
            dis = abs(game.ball.rect.centerx - game.paddle.rect.centerx)
            self.reward_based_on_distance(dis, game)

    def reward_based_on_distance(self, dis, game):
        reward_to_give = 4 - (dis / 5)
        game.reward += reward_to_give 

        # default
        # reward_to_give = 4 - (dis / 5)
        # game.reward += reward_to_give 

    def __check_brick_collision(self, ball, brick):
        collision_thresh = 6

        #check top
        if abs(ball.rect.bottom - brick.rect.top) <= collision_thresh and ball.velocity[1] > 0:
            ball.velocity[1] = -ball.velocity[1]
        #check below
        if abs(ball.rect.top - brick.rect.bottom) <= collision_thresh and ball.velocity[1] < 0:
            ball.velocity[1] = -ball.velocity[1]                    
        #check left
        if abs(ball.rect.right - brick.rect.left) <= collision_thresh and ball.velocity[0] > 0:
            ball.velocity[0] = -ball.velocity[0]
        #check right
        if abs(ball.rect.left - brick.rect.right) <= collision_thresh and ball.velocity[0] < 0:
            ball.velocity[0] = -ball.velocity[0]

    def __add_AI_punishment(self, ball, paddle, game: Game):
        dis = abs(ball.rect.centerx - paddle.rect.centerx)
        game.reward -= (dis * 10)
