from ...breakout.logic.player_input import PlayerInput
from ...breakout.logic.game_logic import GameLogic
from ...breakout.game import Game
from ...breakout.logic.brick_setup import *
from ...breakout.constants.colors import *
from ...breakout.constants.game_constants import *
from ...breakout.constants.screen_constants import *
from ...breakout.screen_manager import ScreenManager
import pygame

class BreakoutGameAI:
    def __init__(self):
        pygame.init()
        self.carryOn = True
        self.reset_game = False

        self.game = Game(pygame)
        self.game.bg = pygame.image.load("src/berserk.jpg")
        self.screen: ScreenManager = ScreenManager()
        self.screen.paused_game = True
        self.gameLogic = GameLogic()
        self.screen.skip_loading = True
    
    def get_ticks(self):
        return self.game.pygame.time.get_ticks() - self.game.last_game_ended

    def play_input(self, action):
        # PlayerInput.process_AI_input(self.paddle, action)

        keys = pygame.key.get_pressed()
        PlayerInput.process_player_input(pygame, self.game.paddle, keys, self.screen)

    def play_step(self, action, number_of_games):
        self.screen.skip_loading = True
        PlayerInput.process_AI_input(self.game, action, self.screen)
        return self.main(number_of_games)

    def main(self, number_of_games):
        self.game.time_since_last_brick_hit += 1
        self.game.total_game_time += 1
        self.game.reward = 0
        
        if (number_of_games % 25 == 0 and number_of_games != 0) or number_of_games >= 0:
            self.screen.update_screen(self.game)
            self.game.all_sprites_list.draw(self.game.screen)
            pygame.display.flip()

        if not self.screen.paused_game:
            self.reset_game = not self.gameLogic.handle_ball_movement(self.game, pygame)
            if self.reset_game:
                self.game.reward += GAME_OVER_PUNISHMENT
                self.screen.paused_game = True
                
                self.reset_game = False
                return self.game.reward, True, self.game.score
            elif self.game.time_since_last_brick_hit > 200*10:
                self.game.reward += TO_LONG_WITHOUT_HIT_PUNISHMENT
                self.screen.paused_game
                return self.game.reward, True, self.game.score

        for event in pygame.event.get(): # User did something
            if event.type == pygame.QUIT: # If user clicked close
                self.carryOn = False
            elif event.type == pygame.KEYDOWN and self.screen.paused_game:
                self.screen.paused_game = False

        self.game.clock.tick(FPS)
        return self.game.reward, False, self.game.score
         
if __name__ == '__main__':
    breakout = BreakoutGameAI()
    
    while breakout.carryOn:
        breakout.play_input(None)
        breakout.main()

    #Stop the engine if out of the loop
    pygame.quit()