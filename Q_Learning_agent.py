from src.breakout.constants.screen_constants import PADDLE_WIDTH, SCREEN_WIDTH
from src.AI.Q_Learning.breakout_AI import BreakoutGameAI
from src.AI.helper import plot
from src.AI.Q_Learning.state_helper import get_state_id, get_version
import random
import numpy as np

TOTAL_STATE_COUNT_DIS_VERSION = round((SCREEN_WIDTH * 2) - PADDLE_WIDTH)  # base 275
q_table = np.zeros((TOTAL_STATE_COUNT_DIS_VERSION, 3)) if get_version() == True else np.zeros((2, 3)) # 3 = actions


class QLearningAgent:
    def __init__(self):
        self.number_of_games = 0
        self.learning_rate = 0.1
        self.discount_factor = 0.1
        self.random_factor = 0.1

    def get_action(self, state_id):
        self.epsilon = 0.2

        action = [0, 0, 0]
        # smaller epsilon the less frequent it will happen (decides a random move or a 'good' move)
        if random.uniform(0, 1) < self.epsilon and self.number_of_games % 2 != 0:
            move = random.randint(0, 2)
            action[move] = 1
        else:
            state_actions = q_table[state_id]
            max_q_index = np.argmax(state_actions)
            action[max_q_index] = 1

        return action

    def calculate_new_q(self, old_q, reward, max_reward_new_state):
        new_q = old_q + (self.learning_rate * (reward +
                         (self.discount_factor * (max_reward_new_state - old_q))))

        return new_q

    def make_a_move(self, breakout: BreakoutGameAI):
        start_state_id = get_state_id(breakout)
        action = self.get_action(start_state_id)
        old_q = q_table[start_state_id][np.argmax(action)]

        reward, game_over, score = breakout.play_step(
            action, self.number_of_games)
        new_state_id = get_state_id(breakout)
        max_reward_new_state = np.amax(q_table[new_state_id])

        q_table[start_state_id][np.argmax(action)] = self.calculate_new_q(
            old_q, reward, max_reward_new_state)

        return reward, game_over, score


def train():
    plot_scores = []
    plot_mean_scores = []
    total_score = 0
    record_score = 0
    agent = QLearningAgent()
    breakout = BreakoutGameAI()

    while True:
        reward, game_over, score = agent.make_a_move(breakout)

        if game_over:
            agent.number_of_games += 1

            if score > record_score:
                record_score = score

            plot_scores.append(score)
            total_score += score
            mean_score = total_score / agent.number_of_games
            plot_mean_scores.append(mean_score)
            plot(plot_scores, plot_mean_scores)
            breakout.gameLogic.reset_game(breakout.game)


if __name__ == '__main__':
    train()
