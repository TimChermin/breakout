from breakout.logic.player_input import PlayerInput
from breakout.logic.game_logic import GameLogic
from breakout.game import Game
import pygame
from breakout.logic.brick_setup import *
from breakout.constants.colors import *
from breakout.constants.constants import *
from breakout.screen_manager import ScreenManager

def main():
    pygame.init()
    carryOn = True
    reset_game = False

    # The clock will be used to control how fast the screen updates
    clock = pygame.time.Clock()
    game = Game(pygame)
    game.bg = pygame.image.load("berserk.jpg")
    screen: ScreenManager = ScreenManager()
    screen.paused_game = True
    gameLogic = GameLogic()

    # -------- Main Program Loop -----------
    while carryOn:
        clock.tick(FPS)
        screen.update_screen(game)
        game.all_sprites_list.draw(game.screen)
        pygame.display.flip()

        keys = pygame.key.get_pressed()
        PlayerInput.process_player_input(pygame, game.paddle, keys, screen)

        if not screen.paused_game:
            reset_game = not gameLogic.handle_ball_movement(game, pygame)
            if reset_game:
                gameLogic.reset_game(game)
                screen.paused_game = True
                reset_game = False

        for event in pygame.event.get(): # User did something
            if event.type == pygame.QUIT: # If user clicked close
                carryOn = False
            elif event.type == pygame.KEYDOWN and screen.paused_game:
                screen.paused_game = False
                
    #Stop the engine if out of the loop
    pygame.quit()

if __name__ == '__main__':
    main()